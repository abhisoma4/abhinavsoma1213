- 👋 Hi, I’m @abhinavsoma1213
- 👀 I’m interested in using AI to help fight real world problems
- 🌱 I’m currently learning Swift, NLP, Web3 Development
- 💞️ I’m looking to collaborate on NLP pet projects and Web3 development ideas
- 📫 How to reach me my email, abhis121213@gmail.com

<!---
abhinavsoma1213/abhinavsoma1213 is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
